<?php

add_action( 'parse_request', 'M150_control_init', 20, 0 );

function M150_control_init() {
    if ( M150_Submission::is_restful() ) {
        return;
    }

    if ( isset( $_POST['_M150'] ) ) {
        $contact_form = M150_contact_form( (int) $_POST['_M150'] );

        if ( $contact_form ) {
            $contact_form->submit();
        }
    }
}

add_action( 'wp_enqueue_scripts', 'M150_do_enqueue_scripts', 10, 0 );

function M150_do_enqueue_scripts() {
    if ( M150_load_js() ) {
        M150_enqueue_scripts();
    }

    if ( M150_load_css() ) {
        M150_enqueue_styles();
    }
}

function M150_enqueue_scripts() {
    $in_footer = true;

    if ( 'header' === M150_load_js() ) {
        $in_footer = false;
    }

    wp_enqueue_script( 'contact-form',
        M150_plugin_url( 'includes/js/scripts.js' ),
        array( 'jquery' ), M150_VERSION, $in_footer );

    $M150 = array(
        'apiSettings' => array(
            'root' => esc_url_raw( rest_url( 'contact-form/v1' ) ),
            'namespace' => 'contact-form/1',
        ),
    );

    if ( defined( 'WP_CACHE' ) and WP_CACHE ) {
        $M150['cached'] = 1;
    }

    if ( M150_support_html5_fallback() ) {
        $M150['jqueryUi'] = 1;
    }

    wp_localize_script( 'contact-form', 'M150', $M150 );

    do_action( 'M150_enqueue_scripts' );
}

function M150_script_is() {
    return wp_script_is( 'contact-form' );
}

function M150_enqueue_styles() {
    wp_enqueue_style( 'contact-form',
        M150_plugin_url( 'includes/css/styles.css' ),
        array(), M150_VERSION, 'all' );

    if ( M150_is_rtl() ) {
        wp_enqueue_style( 'contact-form-rtl',
            M150_plugin_url( 'includes/css/styles-rtl.css' ),
            array(), M150_VERSION, 'all' );
    }

    do_action( 'M150_enqueue_styles' );
}

function M150_style_is() {
    return wp_style_is( 'contact-form' );
}

/* HTML5 Fallback */

add_action( 'wp_enqueue_scripts', 'M150_html5_fallback', 20, 0 );

function M150_html5_fallback() {
    if ( ! M150_support_html5_fallback() ) {
        return;
    }

    if ( M150_script_is() ) {
        wp_enqueue_script( 'jquery-ui-datepicker' );
        wp_enqueue_script( 'jquery-ui-spinner' );
    }

    if ( M150_style_is() ) {
        wp_enqueue_style( 'jquery-ui-smoothness',
            M150_plugin_url(
                'includes/js/jquery-ui/themes/smoothness/jquery-ui.min.css' ),
            array(), '1.11.4', 'screen' );
    }
}
